# MarketExchange

The application is developed using JavaFX 12.

**A short review what you can manage to do with this application:**
*  Display different type of XY chart types (e.g Line, Area, Bar, Scatter chart, and so on...)
*  The user can select text and other file formats like txt, csv, xls to display it's content in a chart. 
*  User can exchange up to 50 different currencies with the latest currency updates. The program is getting the exchange rates using web APIs.
    * Exchange rates are provided by: www.exchangerate-api.com, https://fixer.io/ and https://api.exchangeratesapi.io/  
*  You can get historical currency datas by filling in the required fields. Based on your selection the data will be displayed in one of the above mentioned chart. 
    *  Required fields: start/end date, base currency, symbol currenc(ies) - you can display multiple currencies if you select more currency
*  If the user loads in data and a chart is succesfully created, a floating pane in the right side will appear showing some basic statistics about the series.
*  More features and echancments will come in the future. 

The application has some bugs because it's currenty under development.

*Note: To see the whole repository switch to "develop" branch.*